import itertools

import numpy as np


class Hist2D:
    """NumPy representation of a ROOT.TH2."""

    def __init__(self, root_hist):
        """Initialize from a ROOT.TH2 histogram."""
        binning_x = np.asarray([
            root_hist.GetXaxis().GetBinLowEdge(b)
            for b in range(1, root_hist.GetNbinsX() + 2)
        ])
        binning_y = np.asarray([
            root_hist.GetYaxis().GetBinLowEdge(b)
            for b in range(1, root_hist.GetNbinsY() + 2)
        ])
        self.binning = (binning_x, binning_y)

        self.contents = np.empty((len(binning_x) + 1, len(binning_y) + 1))
        self.errors = np.empty_like(self.contents)

        for ix, iy in itertools.product(
            range(len(binning_x) + 1), range(len(binning_y) + 1)
        ):
            self.contents[ix, iy] = root_hist.GetBinContent(ix, iy)
            self.errors[ix, iy] = root_hist.GetBinError(ix, iy)


    @property
    def numbins(self):
        """Return numbers of bins along x and y axes.
        
        Under- and overflows are not included.
        """
        return len(self.binning[0]) - 1, len(self.binning[1]) - 1


mpl_style = {
    'figure.figsize': (6.0, 4.8),
    
    'axes.labelsize':              'large',
    'axes.formatter.use_mathtext': True,
    'axes.formatter.limits':       (-2, 4),
    
    'xtick.top':          True,
    'xtick.direction':    'in',
    'xtick.minor.top':    True,
    'xtick.minor.bottom': True,
    
    'ytick.right':        True,
    'ytick.direction':    'in',
    'ytick.minor.left':   True,
    'ytick.minor.right':  True,
    
    'lines.linewidth':   1.,
    'lines.markersize':  3.,
    'errorbar.capsize':  1.5
}
