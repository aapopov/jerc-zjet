#!/usr/bin/env python

"""Measures jet pt resolution using Z+jet events."""

from array import array
import argparse
import itertools
import math
import os
from uuid import uuid4

import ROOT
ROOT.PyConfig.IgnoreCommandLineOptions = True

# RooFit
ROOT.gSystem.Load('libRooFit.so')
ROOT.gSystem.Load('libRooFitCore.so')
ROOT.gROOT.SetStyle('Plain')  # Not sure this is needed
ROOT.gSystem.SetIncludePath('-I$ROOFITSYS/include/')


def ConvFit(ptbalance, pu, pli, jer, output, binning, pt_range, eta_range,
            abs_eta, isData=False):

    # Adjust normalizations of input histograms to help with the
    # convergence of the fit
    norm = ptbalance.Integral()
    for hist in [pu, pli, jer]:
        hist.Scale(norm / hist.Integral())

    # Declare the observable mean, and import the histogram to a RooDataHist
    if binning == 'zpt':
        balance = ROOT.RooRealVar('balance', 'p_{T}_{Jet}/p_{T}_{Z}', 0.4, 1.3)
        variable_name = 'p_{T}^{Z}'
    else:
        balance = ROOT.RooRealVar('balance', 'p_{T}_{Jet}/p_{T}_{Z}', 0.5, 1.8)
        variable_name = 'p_{T}^{jet}'
    dh_ptbalance = ROOT.RooDataHist(
        'dh_ptbalance',
        'dh_ptbalance',
        ROOT.RooArgList(balance),
        ROOT.RooFit.Import(ptbalance))

    # plot the data hist with error from sum of weighted events
    frame = balance.frame(ROOT.RooFit.Title('ptbalance'))
    if isData:
        dh_ptbalance.plotOn(
            frame, ROOT.RooFit.DataError(ROOT.RooAbsData.Poisson))
    else:
        dh_ptbalance.plotOn(
            frame, ROOT.RooFit.DataError(ROOT.RooAbsData.SumW2))

    # Now import the templates
    dh_pu = ROOT.RooDataHist('dh_pu', 'dh_pu', ROOT.RooArgList(
        balance), ROOT.RooFit.Import(pu))
    dh_pli = ROOT.RooDataHist('dh_pli', 'dh_pli', ROOT.RooArgList(
        balance), ROOT.RooFit.Import(pli))
    dh_jer = ROOT.RooDataHist('dh_jer', 'dh_jer', ROOT.RooArgList(
        balance), ROOT.RooFit.Import(jer))

    pdf_pu = ROOT.RooHistPdf(
        'pdf_pu', 'pdf_pu', ROOT.RooArgSet(balance), dh_pu)
    pdf_pli = ROOT.RooHistPdf('pdf_pli', 'pdf_pli',
                              ROOT.RooArgSet(balance), dh_pli)
    pdf_jer = ROOT.RooHistPdf('pdf_jer', 'pdf_jer',
                              ROOT.RooArgSet(balance), dh_jer)

    # create a simple gaussian pdf
    gauss_mean = ROOT.RooRealVar('mean', 'mean', 0, -0.3, 0.3)
    gauss_sigma = ROOT.RooRealVar(
        'sigma jer', 'sigma gauss', ptbalance.GetRMS(), 0.05, 2.)
    gauss = ROOT.RooGaussian('gauss', 'gauss', balance,
                             gauss_mean, gauss_sigma)

    # right now don't know how to do a 3 component convolution so only taking
    # pli and gauss
    tmpxg = ROOT.RooFFTConvPdf(
        'tmpxg', 'templates x gauss', balance, pdf_pli, gauss)
    tmpxg.setBufferFraction(0.3)

    # To let the fractions be calculated on the fly, need to convert them to
    # extended pdfs
    nentries = ptbalance.Integral()
    npu = ROOT.RooRealVar('npu', 'npu', 0, nentries)
    n = ROOT.RooRealVar('n', 'n', 0, 0, nentries)

    extpdf_pu = ROOT.RooExtendPdf('extpdf_pu', 'extpdf_pu', pdf_pu, npu)
    extpdf_pli = ROOT.RooExtendPdf('extpdf_pli', 'extpdf_pli', pdf_pli, n)
    extpdf_tmpxg = ROOT.RooExtendPdf('extpdf_tmpxg', 'extpdf_tmpxg', tmpxg, n)

    tmpfit = ROOT.RooAddPdf(
        'tmpfit', 'tmpfit', ROOT.RooArgList(extpdf_pu, extpdf_tmpxg))
    fit_result = tmpfit.fitTo(
        dh_ptbalance, ROOT.RooFit.SumW2Error(True),
        ROOT.RooFit.Minimizer('Minuit2', 'migrad'), ROOT.RooFit.Offset(True),
        ROOT.RooFit.Save(), ROOT.RooFit.PrintLevel(-1)
    )

    if fit_result.status() != 0:
        print(
            'Fit for {} has not converged fully. Status is {}.'.format(
                'data' if isData else 'simulation',
                fit_result.status())
        )

    tmpfit.plotOn(frame)
    tmpfit.plotOn(frame, ROOT.RooFit.Components('extpdf_pu'),
                  ROOT.RooFit.LineStyle(2), ROOT.RooFit.LineColor(8))
    tmpfit.plotOn(frame, ROOT.RooFit.Components('extpdf_tmpxg'),
                  ROOT.RooFit.LineStyle(2), ROOT.RooFit.LineColor(2))

    argset_fit = ROOT.RooArgSet(gauss_mean, gauss_sigma)
    frame.SetMaximum(frame.GetMaximum() * 1.25)

    # add chi2 info
    chi2_text = ROOT.TPaveText(0.15, 0.72, 0.15, 0.88, 'NBNDC')
    chi2_text.SetTextAlign(11)
    chi2_text.AddText('#chi^{2} fit = %s' % round(frame.chiSquare(6), 2))
    chi2_text.AddText('#sigma_{gauss} ' + '= {} #pm {}'.format(
        round(gauss_sigma.getVal(), 3), round(gauss_sigma.getError(), 3)))
    chi2_text.AddText('#mu_{gauss} ' + '= {} #pm {}'.format(
        round(gauss_mean.getVal(), 3), round(gauss_mean.getError(), 3)))
    chi2_text.SetTextSize(0.03)
    chi2_text.SetTextColor(2)
    chi2_text.SetShadowColor(0)
    chi2_text.SetFillColor(0)
    chi2_text.SetLineColor(0)
    frame.addObject(chi2_text)

    cfit = ROOT.TCanvas('cfit', 'cfit', 600, 600)
    frame.Draw()

    latex2 = ROOT.TLatex()
    latex2.SetNDC()
    latex2.SetTextSize(0.3 * cfit.GetTopMargin())
    latex2.SetTextFont(42)
    latex2.SetTextAlign(31)  # align right

    bin_label = \
        '{min_pt:g} < {pt} < {max_pt:g} GeV, ' \
        '{min_eta:g} < {eta} < {max_eta:g}'.format(
            min_pt=pt_range[0], max_pt=pt_range[-1], pt=variable_name,
            min_eta=eta_range[0], max_eta=eta_range[-1],
            eta='|#eta_{jet}|' if abs_eta else '#eta_{jet}'
        )

    if isData:
        bin_label += ', Data'
    else:
        bin_label += ', Sim.'

    latex2.DrawLatex(0.90, 0.93, bin_label)
    latex2.Draw('same')

    legend = ROOT.TLegend(0.60, 0.75, 0.88, 0.88)
    legend.AddEntry(frame.findObject(
        'tmpfit_Norm[balance]_Comp[extpdf_pu]'), 'pile up', 'l')
    legend.AddEntry(frame.findObject(
        'tmpfit_Norm[balance]_Comp[extpdf_tmpxg]'), 'pli #otimes gauss', 'l')
    legend.SetFillColor(0)
    legend.SetLineColor(0)
    legend.Draw('same')

    fit_filename = 'fit_pt{}to{}_eta{}to{}'.format(
        round(pt_range[0]), round(pt_range[-1]),
        str(eta_range[0]).replace('.', 'p').replace('-', 'min'),
        str(eta_range[-1]).replace('.', 'p').replace('-', 'min')
    )
    
    for ext in ['pdf', 'svg']:
        cfit.SaveAs(os.path.join(
            output,
            '{}_{}.{}'.format(fit_filename, 'data' if isData else 'sim', ext)
        ))

    return gauss_sigma.getVal(), gauss_sigma.getError(
    ), gauss_mean.getVal(), gauss_mean.getError()


def main():
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('data', help='File with histograms for data.')
    parser.add_argument('sim', help='File with histograms for simulation.')
    parser.add_argument('-o', '--output', default='fig',
                        help='Name for output directory with plots')
    args = parser.parse_args()

    try:
        os.makedirs(args.output + '/bins')
    except FileExistsError:
        pass


    ROOT.gStyle.SetOptStat(0)
    ROOT.gStyle.SetPaintTextFormat('4.2f')
    ROOT.gErrorIgnoreLevel = ROOT.kWarning
    ROOT.gROOT.SetBatch(True)
    ROOT.RooMsgService.instance().setGlobalKillBelow(ROOT.RooFit.FATAL)
    ROOT.TH1.AddDirectory(False)
    ROOT.TH1.SetDefaultSumw2()

    # Source 3D histograms in pt(jet), |eta(jet)|, pt(jet)/pt(Z)
    input_file = ROOT.TFile(args.data)
    data_nominal = input_file.Get('Nominal')
    data_pileup = input_file.Get('Pileup')
    for hist in [data_nominal, data_pileup]:
        hist.SetDirectory(None)
    input_file.Close()

    input_file = ROOT.TFile(args.sim)
    sim_nominal = input_file.Get('Nominal')
    sim_pileup = input_file.Get('Pileup')
    sim_generator = input_file.Get('Generator')
    sim_smear = input_file.Get('Smear')
    for hist in [sim_nominal, sim_pileup, sim_generator, sim_smear]:
        hist.SetDirectory(None)
    input_file.Close()


    # Extract binning in pt and eta
    pt_binning = array('d', [
        sim_nominal.GetXaxis().GetBinLowEdge(b)
        for b in range(1, sim_nominal.GetNbinsX() + 2)
    ])
    eta_binning = array('d', [
        sim_nominal.GetYaxis().GetBinLowEdge(b)
        for b in range(1, sim_nominal.GetNbinsY() + 2)
    ])


    hdata = ROOT.TH2F(
        'JER_data', 'JER in data',
        len(pt_binning) - 1, pt_binning, len(eta_binning) - 1, eta_binning)
    hmc = hdata.Clone('JER_sim')
    hmc.SetTitle('JER in simulation')

    hjer_sf = hdata.Clone('JER_SF')
    hjer_sf.SetTitle('JER scale factor')

    hjec_data = hdata.Clone('mean_data')
    hjec_data.SetTitle('#mu in data')
    hjec_mc = hjec_data.Clone('mean_sim')
    hjec_mc.SetTitle('#mu in simulation')

    hresidual = hjec_data.Clone('mean_diff')
    hresidual.SetTitle('#mu_{data} - #mu_{sim}')

    for hist in [hdata, hmc, hjer_sf, hjec_data, hjec_mc, hresidual]:
        hist.GetXaxis().SetTitle('Jet p_{T} [GeV]')
        hist.GetYaxis().SetTitle('Jet #eta')


    for ipt, ieta in itertools.product(
        range(len(pt_binning) - 1), range(len(eta_binning) - 1)
    ):
        pt_range = (pt_binning[ipt], pt_binning[ipt + 1])
        eta_range = (eta_binning[ieta], eta_binning[ieta + 1])
        print(
            'Performing fits in bin {0:g} < pt < {1:g} GeV, '
            '{2:g} < |eta| < {3:g}'.format(
                pt_range[0], pt_range[-1],
                eta_range[0], eta_range[-1]
            )
        )
        def project(hist3d):
            return hist3d.ProjectionZ(
                uuid4().hex, ipt + 1, ipt + 1, ieta + 1, ieta + 1, 'e')

        h_ptbal = project(sim_nominal)
        h_ptbal_pu = project(sim_pileup)
        h_pli = project(sim_generator)
        h_jer = project(sim_smear)

        h_ptbaldata = project(data_nominal)
        h_ptbal_pudata = project(data_pileup)

        sigma, sigma_error, mean, mean_error = ConvFit(
            h_ptbal, h_ptbal_pu, h_pli, h_jer, args.output + '/bins',
            'jetpt', pt_range, eta_range, abs_eta=True)
        sigma_data, sigma_error_data, mean_data, mean_error_data = ConvFit(
            h_ptbaldata, h_ptbal_pudata, h_pli, h_jer, args.output + '/bins',
            'jetpt', pt_range, eta_range, abs_eta=True, isData=True)

        hmc.SetBinContent(ipt + 1, ieta + 1, sigma)
        hmc.SetBinError(ipt + 1, ieta + 1, sigma_error)

        hdata.SetBinContent(ipt + 1, ieta + 1, sigma_data)
        hdata.SetBinError(ipt + 1, ieta + 1, sigma_error_data)

        hjec_mc.SetBinContent(ipt + 1, ieta + 1, mean)
        hjec_mc.SetBinError(ipt + 1, ieta + 1, mean_error)

        hjec_data.SetBinContent(ipt + 1, ieta + 1, mean_data)
        hjec_data.SetBinError(ipt + 1, ieta + 1, mean_error_data)

        hresidual.SetBinContent(ipt + 1, ieta + 1, mean_data - mean)
        hresidual.SetBinError(
            ipt + 1, ieta + 1,
            math.sqrt(mean_error_data ** 2 + mean_error ** 2))
        print()

    hjer_sf.Divide(hdata, hmc)

    output_file = ROOT.TFile(
        os.path.join(args.output, 'results.root'), 'recreate')

    for hist in [hdata, hmc, hjer_sf, hjec_data, hjec_mc, hresidual]:
        hist.SetDirectory(output_file)

    output_file.Write()
    output_file.Close()


if __name__ == '__main__':
    main()

