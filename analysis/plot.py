#!/usr/bin/env python

"""Plots results of JER measurement."""

import argparse
import itertools
import math
import os

import numpy as np

import matplotlib as mpl
mpl.use('agg')
from matplotlib import pyplot as plt

import ROOT
ROOT.PyConfig.IgnoreCommandLineOptions = True

from utils import Hist2D, mpl_style


def plot_root_hist2d(hist, filename, zlim=(None, None)):
    """Plot a ROOT.TH2.
    
    Use log scale for x axis.

    Arguments:
        hist:  ROOT histogram.
        filename:  Name for file with produced figure, without
            extension.
        zlim:  Range for z axis.

    Return value:
        None.
    """

    canvas = ROOT.TCanvas('canvas', '', 635, 600)
    canvas.SetLogx()
    canvas.SetRightMargin(0.15)

    if zlim[0]:
        hist.SetMinimum(zlim[0])
    if zlim[1]:
        hist.SetMaximum(zlim[1])

    hist.GetXaxis().SetMoreLogLabels()
    hist.Draw('colz text')

    for ext in ['pdf', 'svg']:
        canvas.SaveAs('{}.{}'.format(filename, ext))


def plot_pt_slices(hist_jer, output_dir, bin_var='jetpt'):
    """Plot JER SF in pt slices.

    Arguments:
        hist_jer:    JER SF represented by a Hist2D.
        output_dir:  Output directory for plots.
        bin_var:     pt binning variable.

    Return value:
        None.
    """

    pt_binning = hist_jer.binning[0]
    eta_binning = hist_jer.binning[1]
    use_abs_eta = (eta_binning[0] >= 0.)

    sf_range = (0.5, 2.5)
    if use_abs_eta:
        xlabel = 'Jet $|\\eta$|'
    else:
        xlabel = 'Jet $\\eta$'

    fig = plt.figure()
    fig.patch.set_alpha(0)
    axes = fig.add_subplot(111)

    color_map = mpl.cm.ScalarMappable(
        mpl.colors.Normalize(-0.5, len(pt_binning) - 0.5), 'viridis')

    for ipt in range(len(pt_binning) - 1):
        jer_sf = np.empty(len(eta_binning) - 1)
        jer_sf_error = np.empty_like(jer_sf)

        for ieta in range(len(eta_binning) - 1):
            jer_sf[ieta] = hist_jer_sf.GetBinContent(ipt + 1, ieta + 1)
            jer_sf_error[ieta] = hist_jer_sf.GetBinError(ipt + 1, ieta + 1)

        color = color_map.to_rgba(ipt)
        label = '${0:g} < p_\\mathrm{{T}}^{2} < {1:g}$ GeV'.format(
            pt_binning[ipt], pt_binning[ipt + 1],
            'Z' if bin_var == 'zpt' else 'j'
        )
        axes.errorbar(
            (eta_binning[:-1] + eta_binning[1:]) / 2,
            hist_jer.contents[ipt + 1, 1:-1],
            yerr=hist_jer.errors[ipt + 1, 1:-1],
            marker='o', mfc=color,
            c=mpl.colors.to_rgba(color, 0.4), ecolor=color,
            label=label
        )

    axes.set_xlim(eta_binning[0], eta_binning[-1])
    axes.set_ylim(*sf_range)

    for eta in eta_binning[1:-1]:
        axes.axvline(eta, c='0.75', ls='dashed', lw=0.8)

    axes.set_xlabel(xlabel)
    axes.set_ylabel('JER SF')
    axes.legend(loc='upper center', ncol=2)
    for ext in ['pdf', 'svg']:
        fig.savefig(os.path.join(output_dir, 'jer_sf_pt-slices.' + ext))
    plt.close(fig)


    # Produce also separate plots for all slices
    for ipt in range(len(pt_binning) - 1):
        fig = plt.figure()
        fig.patch.set_alpha(0.)
        axes = fig.add_subplot(111)

        axes.errorbar(
            (eta_binning[:-1] + eta_binning[1:]) / 2,
            hist_jer.contents[ipt + 1, 1:-1],
            yerr=hist_jer.errors[ipt + 1, 1:-1],
            marker='o', lw=0, elinewidth=1.)

        axes.set_xlim(eta_binning[0], eta_binning[-1])
        axes.set_ylim(*sf_range)

        for eta in eta_binning[1:-1]:
            axes.axvline(eta, c='0.75', ls='dashed', lw=0.8)

        axes.set_xlabel(xlabel)
        axes.set_ylabel('JER SF')
        axes.text(
            1., 1.002,
            '${0:g} < p_\\mathrm{{T}}^{2} < {1:g}$ GeV'.format(
                pt_binning[ipt], pt_binning[ipt + 1],
                'Z' if bin_var == 'zpt' else 'j'),
            ha='right', va='bottom', transform=axes.transAxes
        )

        for ext in ['pdf', 'svg']:
            fig.savefig(os.path.join(
                output_dir, 'slices',
                'jer_sf_pt-bin{:02d}.{}'.format(ipt + 1, ext)
            ))
        plt.close(fig)


def plot_eta_slices(hist_jer, output_dir, bin_var='jetpt'):
    """Plot JER SF in eta slices.

    Arguments:
        hist_jer:    JER SF represented by a Hist2D.
        output_dir:  Output directory for plots.
        bin_var:     pt binning variable.

    Return value:
        None.
    """

    pt_binning = hist_jer.binning[0]
    eta_binning = hist_jer.binning[1]
    use_abs_eta = (eta_binning[0] >= 0.)

    sf_range = (0.5, 2.5)
    if bin_var == 'zpt':
        xlabel = 'Z $p_\\mathrm{T}$ [GeV]'
    else:
        xlabel = 'Jet $p_\\mathrm{T}$ [GeV]'

    fig = plt.figure()
    fig.patch.set_alpha(0)
    axes = fig.add_subplot(111)

    if use_abs_eta:
        num_colors = len(eta_binning) - 1
    else:
        num_colors = (len(eta_binning) - 1) // 2

    color_map = mpl.cm.ScalarMappable(
        mpl.colors.Normalize(-0.5, num_colors - 0.5), 'viridis')

    for ieta in range(len(eta_binning) - 1):
        if use_abs_eta:
            marker = 'o'
            color = color_map.to_rgba(ieta)
            label = '${0:g} < |\\eta| < {1:g}$'.format(
                eta_binning[ieta], eta_binning[ieta + 1]
            )
        else:
            pos_eta = ieta >= (len(eta_binning) - 1) // 2
            marker = '^' if pos_eta else 'v'
            color = color_map.to_rgba(
                abs(ieta - (len(eta_binning) - 1) / 2 + 0.5) - 0.5
            )
            if pos_eta:
                label = '${0:g} < |\\eta| < {1:g}$'.format(
                    eta_binning[ieta], eta_binning[ieta + 1]
                )
            else:
                label = None

        axes.errorbar(
            (pt_binning[:-1] + pt_binning[1:]) / 2,
            hist_jer.contents[1:-1, ieta + 1],
            yerr=hist_jer.errors[1:-1, ieta + 1],
            marker=marker, mfc=color,
            c=mpl.colors.to_rgba(color, 0.4), ecolor=color,
            label=label
        )

    axes.set_xlim(pt_binning[0], pt_binning[-1])
    axes.set_ylim(*sf_range)
    axes.set_xscale('log')

    for pt in pt_binning[1:-1]:
        axes.axvline(pt, c='0.75', ls='dashed', lw=0.8)

    axes.set_xlabel(xlabel)
    axes.set_ylabel('JER SF')
    axes.legend(loc='upper center', ncol=2)
    for ext in ['pdf', 'svg']:
        fig.savefig(os.path.join(output_dir, 'jer_sf_eta-slices.' + ext))
    plt.close(fig)


    # Produce also separate plots for all slices
    if not use_abs_eta:
        eta_bin_groups = []
        n = len(eta_binning) - 1
        for i in range(n // 2):  # Number of bins is necessarily even
            eta_bin_groups.append([
                (n // 2 + i, '^', '$\eta > 0$'),
                (n // 2 - i - 1, 'v', '$\eta < 0$')
            ])
    else:
        eta_bin_groups = [
            [(ieta, 'o', '')] for ieta in range(len(eta_binning) - 1)]

    for igroup, group in enumerate(eta_bin_groups):
        fig = plt.figure()
        fig.patch.set_alpha(0.)
        axes = fig.add_subplot(111)

        for ieta, marker, label in group:
            axes.errorbar(
                (pt_binning[:-1] + pt_binning[1:]) / 2,
                hist_jer.contents[1:-1, ieta + 1],
                yerr=hist_jer.errors[1:-1, ieta + 1],
                marker=marker, lw=0, elinewidth=1, label=label
            )

        axes.set_xlim(pt_binning[0], pt_binning[-1])
        axes.set_ylim(*sf_range)
        axes.set_xscale('log')

        for pt in pt_binning[1:-1]:
            axes.axvline(pt, c='0.75', ls='dashed', lw=0.8)

        axes.set_xlabel(xlabel)
        axes.set_ylabel('JER SF')
        axes.text(
            1., 1.002, '${:g} < |\eta| < {:g}$'.format(
                eta_binning[group[0][0]], eta_binning[group[0][0] + 1]),
            ha='right', va='bottom', transform=axes.transAxes
        )
        if len(group) > 1:
            axes.legend(loc='upper left')

        for ext in ['pdf', 'svg']:
            fig.savefig(os.path.join(
                output_dir, 'slices',
                'jer_sf_eta-bin{:02d}.{}'.format(igroup + 1, ext)
            ))
        plt.close(fig)



if __name__ == '__main__':
    arg_parser = argparse.ArgumentParser(description=__doc__)
    arg_parser.add_argument('input_file',
                        help='ROOT file with results of the measurement')
    arg_parser.add_argument('-o', '--output', default='output',
                        help='Name for output directory with plots')
    args = arg_parser.parse_args()

    try:
        os.makedirs(args.output + '/slices')
    except FileExistsError:
        pass

    plt.style.use(mpl_style)
    ROOT.gStyle.SetOptStat(0)
    ROOT.gStyle.SetPaintTextFormat('4.2f')
    ROOT.gROOT.SetBatch(True)
    ROOT.gErrorIgnoreLevel = ROOT.kWarning
    ROOT.TH1.AddDirectory(False)
    ROOT.TH1.SetDefaultSumw2()


    input_file = ROOT.TFile(args.input_file)

    hist_jer_data = input_file.Get('JER_data')
    hist_jer_sim = input_file.Get('JER_sim')
    hist_jer_sf = input_file.Get('JER_SF')

    hist_mean_data = input_file.Get('mean_data')
    hist_mean_sim = input_file.Get('mean_sim')
    hist_mean_diff = input_file.Get('mean_diff')

    for hist in [
        hist_jer_data, hist_jer_sim, hist_jer_sf,
        hist_mean_data, hist_mean_sim, hist_mean_diff
    ]:
        hist.SetDirectory(None)

    input_file.Close()

    plot_root_hist2d(hist_jer_data, os.path.join(args.output, 'jer_data'),
                     zlim=(0., 0.5))
    plot_root_hist2d(hist_jer_sim, os.path.join(args.output, 'jer_sim'),
                     zlim=(0., 0.5))
    plot_root_hist2d(hist_jer_sf, os.path.join(args.output, 'jer_sf'),
                     zlim=(0.5, 2.))

    plot_root_hist2d(hist_mean_data, os.path.join(args.output, 'mean_data'),
                     zlim=(-0.15, 0.15))
    plot_root_hist2d(hist_mean_sim, os.path.join(args.output, 'mean_sim'),
                     zlim=(-0.15, 0.15))
    plot_root_hist2d(hist_mean_diff, os.path.join(args.output, 'mean_diff'),
                     zlim=(-0.1, 0.1))


    x_axis_label = hist_jer_sf.GetXaxis().GetTitle()

    if x_axis_label.startswith('Z '):
        bin_var = 'zpt'
    elif x_axis_label.startswith('Jet '):
        bin_var = 'jetpt'
    else:
        raise RuntimeError('Failed to deduce pt binning variable.')


    nphist_jer_sf = Hist2D(hist_jer_sf)
    plot_pt_slices(nphist_jer_sf, args.output, bin_var)
    plot_eta_slices(nphist_jer_sf, args.output, bin_var)

