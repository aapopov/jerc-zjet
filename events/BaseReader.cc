#include <BaseReader.h>

#include <algorithm>
#include <array>
#include <cmath>
#include <cstdlib>
#include <limits>
#include <iostream>
#include <sstream>
#include <stdexcept>

#include <TVector2.h>


Jet::Jet(double pt_, double eta_, double phi_, double pt_gen_,
         double pileup_id_discr)
    : pt(pt_), eta(eta_), phi(phi_), pt_gen(pt_gen_) {
  
  // Pileup ID workping points are chosen according to [1]
  // [1] https://twiki.cern.ch/twiki/bin/viewauth/CMS/PileupJetID?rev=45#Information_for_13_TeV_data_anal
  if (pt > 50.)
    pileup_id = PuIdWp::kTight;
  else {
    double const abs_eta = std::abs(eta);
    int eta_bin;
    if (abs_eta < 2.5)
      eta_bin = 0;
    else if (abs_eta < 2.75)
      eta_bin = 1;
    else if (abs_eta < 3.)
      eta_bin = 2;
    else
      eta_bin = 3;

    int const pt_bin = (pt < 30.) ? 0 : 1;

    std::array<std::array<std::array<double, 4>, 2>, 3> cuts = {{
      {{{{-0.97, -0.68, -0.53, -0.47}},    // Loose, low pt
        {{-0.89, -0.52, -0.38, -0.30}}}},  // Loose, high pt
      {{{{0.18, -0.55, -0.42, -0.36}},     // Medium, low pt
        {{0.61, -0.35, -0.23, -0.17}}}},   // Medium, high pt
      {{{{0.69, -0.35, -0.26, -0.21}},     // Tight, low pt
        {{0.86, -0.10, -0.05, -0.01}}}}    // Tight, high pt
    }};
    if (pileup_id_discr > cuts[2][pt_bin][eta_bin])
      pileup_id = PuIdWp::kTight;
    else if (pileup_id_discr > cuts[1][pt_bin][eta_bin])
      pileup_id = PuIdWp::kMedium;
    else if (pileup_id_discr > cuts[0][pt_bin][eta_bin])
      pileup_id = PuIdWp::kLoose;
    else
      pileup_id = PuIdWp::kNone;
  }
}


BaseReader::BaseReader(std::vector<std::filesystem::path> const &paths,
                       bool read_gen, int64_t events_to_read)
    : chain_{"FakeElectrons/fakeTree"},
      reader_{&chain_},
      print_progress_{false},
      lepton_pt_{reader_, "_lPt"},
      lepton_eta_{reader_, "_lEta"},
      lepton_phi_{reader_, "_lPhi"},
      lepton_flavor_{reader_, "_flavors"},
      lepton_charge_{reader_, "_charges"},
      lepton_tight_id_{reader_, "_istightIDWP2015_RA7"},
      jet_pt_{reader_, "_jetPt"},
      jet_eta_{reader_, "_jetEta"},
      jet_phi_{reader_, "_jetPhi"},
      jet_pt_gen_{reader_, "_jetPtGen"},
      jet_id_{reader_, "_jetPassID"},
      jet_pileup_id_{reader_, "_jetPUMVA"},
      weight_{reader_, "_weight"},
      events_to_read_{events_to_read}, events_read_{0}, events_accepted_{0} {

  if (read_gen) {
    gen_lepton_pt_.emplace(reader_, "_lgenPt");
    gen_lepton_eta_.emplace(reader_, "_lgenEta");
    gen_lepton_phi_.emplace(reader_, "_lgenPhi");
    gen_lepton_pdg_id_.emplace(reader_, "_lgenpdgId");
  }

  for (auto const &path : paths)
    chain_.Add(path.c_str());

  if (events_to_read_ == -1)
    events_to_read_ = chain_.GetEntries();
}


bool BaseReader::Next() {
  while (reader_.Next()) {
    ++events_read_;
    if (events_read_ > events_to_read_)
      return false;
    if (events_read_ % 1'000'000 == 0)
      std::cout << "Processed " << events_read_ << " events out of "
          << events_to_read_ << " requested." << std::endl;

    if (not Build())
      continue;
    
    ++events_accepted_;
    return true;
  }

  return false;
}


std::optional<TLorentzVector> BaseReader::ReconstructGenZ() const {
  TLorentzVector p4_z;
  double min_distance = std::numeric_limits<double>::infinity();

  for (int i = 0; i < int(gen_lepton_pt_->GetSize()) - 1; ++i)
    for (int j = i + 1; j < int(gen_lepton_pt_->GetSize()); ++j) {
      if ((*gen_lepton_pdg_id_)[i] != -(*gen_lepton_pdg_id_)[j])
        continue;
      TLorentzVector const p4_ll = BuildGenLeptonP4(i) + BuildGenLeptonP4(j);

      double const distance = std::abs(p4_ll.M() - 91.1876);
      if (distance < min_distance) {
        min_distance = distance;
        p4_z = p4_ll;
      }
    }

  if (std::isfinite(min_distance))
    return p4_z;
  else
    return {};
}


bool BaseReader::Build() {
  // Mask out leptons that fail ID or have too low a pt
  lepton_mask_.clear();

  for (unsigned i = 0; i < lepton_pt_.GetSize(); ++i) {
    double const min_pt = (lepton_flavor_[i] == 0) ? 15. : 10.;
    lepton_mask_.push_back(lepton_pt_[i] > min_pt and lepton_tight_id_[i]);
  }


  // Reconstruct Z candidate. Make sure that exactly one is found.
  int num_z = 0;

  for (unsigned i = 0; i < lepton_pt_.GetSize() - 1; ++i) {
    if (not lepton_mask_[i])
      continue;

    for (unsigned j = i + 1; j < lepton_pt_.GetSize(); ++j) {
      if (not lepton_mask_[j])
        continue;

      if (lepton_flavor_[i] != lepton_flavor_[j])
        continue;

      if (lepton_charge_[i] == lepton_charge_[j])
        continue;

      if (lepton_flavor_[i] == 0) {
        // Electrons
        if (lepton_pt_[i] < 25. and lepton_pt_[j] < 25.)
          continue;
      } else {
        // Muons
        if (lepton_pt_[i] < 20. and lepton_pt_[j] < 20.)
          continue;
      }

      TLorentzVector const p4_dilepton =
        BuildLeptonP4(i) + BuildLeptonP4(j);

      if (p4_dilepton.M() > 70. and p4_dilepton.M() < 110.) {
        ++num_z;
        p4_z_ = p4_dilepton;
      }
    }
  }

  if (num_z != 1)
    return false;


  // Identify jets overlapping with leptons
  jet_mask_.clear();

  for (unsigned ijet = 0; ijet < jet_pt_.GetSize(); ++ijet) {
    bool has_overlap = false;

    for (unsigned ilepton = 0; ilepton < lepton_pt_.GetSize();
       ++ilepton) {
      if (not lepton_mask_[ilepton])
        continue;

      double const deta = jet_eta_[ijet] - lepton_eta_[ilepton];
      double const dphi = TVector2::Phi_mpi_pi(
        jet_phi_[ijet] - lepton_phi_[ilepton]);
      double const dr2 = std::pow(deta, 2) + std::pow(dphi, 2);

      if (dr2 < std::pow(0.4, 2)) {
        has_overlap = true;
        break;
      }
    }

    jet_mask_.push_back(not has_overlap);
  }


  // Reject events that contain jets that fail the jet ID
  for (unsigned i = 0; i < jet_pt_.GetSize(); ++i) {
    if (not jet_mask_[i] or jet_pt_[i] < 15.)
      continue;
    if (not jet_id_[i])
      return false;
  }


  // Count jets with descent pt
  int num_hard_jets = 0;
  for (unsigned i = 0; i < jet_pt_.GetSize(); ++i) {
    if (not jet_mask_[i])
      continue;
    double const abs_eta = std::abs(jet_eta_[i]);
    if (abs_eta < 2.4 and jet_pt_[i] > 20.)
      ++num_hard_jets;
    else if (abs_eta < 5. and jet_pt_[i] > 30.)
      ++num_hard_jets;
  }
  if (num_hard_jets > 1)
    return false;


  jets_.clear();
  for (unsigned i = 0; i < jet_pt_.GetSize(); ++i) {
    if (not jet_mask_[i])
      continue;
    jets_.emplace_back(
        jet_pt_[i], jet_eta_[i], jet_phi_[i], jet_pt_gen_[i],
        jet_pileup_id_[i]);
  }
  std::sort(jets_.rbegin(), jets_.rend(),
            [](Jet const &j1, Jet const &j2){return j1.pt < j2.pt;});

  return true;
}


TLorentzVector BaseReader::BuildGenLeptonP4(int index) const {
  double mass;
  switch (std::abs((*gen_lepton_pdg_id_)[index])) {
    case 13:
      mass = 0.106;
      break;
    case 15:
      mass = 1.777;
      break;
    default:
      mass = 0.;
  }

  TLorentzVector p4;
  p4.SetPtEtaPhiM(
    (*gen_lepton_pt_)[index], (*gen_lepton_eta_)[index],
    (*gen_lepton_phi_)[index], mass);
  return p4;
}


TLorentzVector BaseReader::BuildLeptonP4(int index) const {
  double const mass = (lepton_flavor_[index] == 0) ? 0. : 0.106;
  TLorentzVector p4;
  p4.SetPtEtaPhiM(
    lepton_pt_[index], lepton_eta_[index], lepton_phi_[index], mass);
  return p4;
}

