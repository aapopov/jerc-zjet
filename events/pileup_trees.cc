#include <filesystem>
#include <vector>

#include <TFile.h>
#include <TTree.h>
#include <TVector2.h>

#include <BaseReader.h>

namespace fs = std::filesystem;


void FillJetDistrs(std::vector<fs::path> const &source_paths,
                   fs::path const &output_path) {
  TFile output_file{output_path.c_str(), "recreate"};
  TTree tree{"Jets", "Pileup jets"};
  tree.SetDirectory(&output_file);
  Float_t jet_pt, jet_eta;
  Int_t jet_pileup_id;
  tree.Branch("pt", &jet_pt);
  tree.Branch("eta", &jet_eta);
  tree.Branch("pileup_id", &jet_pileup_id);

  BaseReader reader{source_paths, false};
  while (reader.Next()) {
    TLorentzVector const &p4_z = reader.GetZ();
    if (p4_z.Pt() > 5 or reader.GetJets().empty())
      continue;

    auto const &jet = reader.GetJets()[0];
    if (jet.pt < 15.)
      continue;

    double const dphi = TVector2::Phi_mpi_pi(p4_z.Phi() - jet.phi);
    if (dphi < 1. or dphi > 2.)
      continue;

    jet_pt = jet.pt;
    jet_eta = jet.eta;
    jet_pileup_id = int(jet.pileup_id);
    tree.Fill();
  }

  tree.Write();
  output_file.Close();
}


void FillZDistrs(fs::path const &source_path, fs::path const &output_path) {
  TFile output_file{output_path.c_str(), "recreate"};
  TTree tree{"Z", "Z boson with pileup jets"};
  tree.SetDirectory(&output_file);
  Float_t pt;
  tree.Branch("pt", &pt);

  BaseReader reader{{source_path}, false};
  while (reader.Next()) {
    bool only_pileup = true;
    for (auto const &jet : reader.GetJets()) {
      if (jet.pt < 15.)
        break;
      if (jet.pt_gen > 0.) {
        only_pileup = false;
        break;
      }
    }
    if (not only_pileup)
      continue;

    pt = reader.GetZ().Pt();
    tree.Fill();
  }

  tree.Write();
  output_file.Close();
}


int main() {
  fs::path const output_dir{"trees"};
  fs::create_directories(output_dir);

  FillJetDistrs(
      {"/pnfs/iihe/cms/store/user/lathomas/DoubleMuon/Run2018A_17Sep2018_v2_Run2018A_SkimDileptonJERC_July2019_LowJetPt/190731_150758/TOTAL.root",
       "/pnfs/iihe/cms/store/user/lathomas/EGamma/Run2018A_17Sep2018_v2_Run2018A_SkimDileptonJERC_July2019_LowJetPt/190731_151920/TOTAL.root"},
      output_dir / "2018A.root");
  FillJetDistrs(
      {"/pnfs/iihe/cms/store/user/lathomas/DoubleMuon/Run2018B_17Sep2018_v1_Run2018B_SkimDileptonJERC_July2019_LowJetPt/190731_151447/TOTAL.root",
       "/pnfs/iihe/cms/store/user/lathomas/EGamma/Run2018B_17Sep2018_v1_Run2018B_SkimDileptonJERC_July2019_LowJetPt/190731_152048/TOTAL.root"},
      output_dir / "2018B.root");
  FillJetDistrs(
      {"/pnfs/iihe/cms/store/user/lathomas/DoubleMuon/Run2018C_17Sep2018_v1_Run2018C_SkimDileptonJERC_July2019_LowJetPt/190731_151619/TOTAL.root",
       "/pnfs/iihe/cms/store/user/lathomas/EGamma/Run2018C_17Sep2018_v1_Run2018C_SkimDileptonJERC_July2019_LowJetPt/190731_152212/TOTAL.root"},
      output_dir / "2018C.root");
  FillJetDistrs(
      {"/pnfs/iihe/cms/store/user/lathomas/DoubleMuon/Run2018D_PromptReco_v2_Run2018D_SkimDileptonJERC_July2019_LowJetPt/190731_151751/TOTAL.root",
       "/pnfs/iihe/cms/store/user/lathomas/EGamma/Run2018D_PromptReco_v2_Run2018D_SkimDileptonJERC_July2019_LowJetPt/190731_152332/TOTAL.root"},
      output_dir / "2018D.root");
  FillJetDistrs(
      {"/pnfs/iihe/cms/store/user/lathomas/DYJetsToLL_M-50_TuneCP5_13TeV-amcatnloFXFX-pythia8/RunIIAutumn18MiniAOD_102X_upgrade2018_realistic_v15_ext2_v1_MC2018_SkimDileptonJERC_July2019_LowJetPt/190731_152454/TOTAL.root"},
      output_dir / "2018Sim.root");

  FillZDistrs(
      "/pnfs/iihe/cms/store/user/lathomas/DYJetsToLL_M-50_TuneCP5_13TeV-amcatnloFXFX-pythia8/RunIIAutumn18MiniAOD_102X_upgrade2018_realistic_v15_ext2_v1_MC2018_SkimDileptonJERC_July2019_LowJetPt/190731_152454/TOTAL.root",
      output_dir / "2018SimZ.root");
  return EXIT_SUCCESS;
}

