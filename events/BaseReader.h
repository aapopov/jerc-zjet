#pragma once

#include <filesystem>
#include <optional>
#include <vector>

#include <TChain.h>
#include <TFile.h>
#include <TLorentzVector.h>
#include <TTreeReader.h>
#include <TTreeReaderArray.h>
#include <TTreeReaderValue.h>


struct Jet {
  enum class PuIdWp: int {
    kNone = 0,  // Fails loose working point
    kLoose = 1,
    kMedium = 2,
    kTight = 3
  };

  Jet(double pt, double eta, double phi, double pt_gen, double pileup_id_discr);

  double pt;
  double eta;
  double phi;
  double pt_gen;

  /**
   * \brief Pileup ID converted to working points
   *
   * Jets with pt > 50 GeV automatically pass the pileup ID and therefore
   * assigned the tight working point.
   */
  PuIdWp pileup_id;
};


/**
 * \brief Implements lepton selection and provides Z boson and jets
 *
 * Events that contain exactly one reconstructed Z boson are selected, but no
 * veto on additional leptons is applied. An event is also rejected if it
 * contains any jets with corrected pt > 15 GeV that fail the jet ID or if there
 * are more than one jet with descent pt.
 */
class BaseReader {
 public:
  BaseReader(std::vector<std::filesystem::path> const &paths, bool read_gen,
             int64_t events_to_read = -1);

  /**
   * \brief Returns jets in the current event
   *
   * They have been cleaned against the leptons and ordered in pt.
   */
  std::vector<Jet> const &GetJets() const {
    return jets_;
  }

  double GetSelEfficiency() const {
    return events_accepted_ / double(events_read_);
  }

  double GetWeight() const {
    return *weight_;
  }

  TLorentzVector const &GetZ() const {
    return p4_z_;
  }
  
  bool Next();

  /**
   * \brief Constructs Z boson candidate from generator-level leptons
   *
   * If there are multiple dilepton pairs, the one whose mass is closest to the
   * PDG value is chosen.
   */
  std::optional<TLorentzVector> ReconstructGenZ() const;

  void SetPrintProgress(bool on = true) {
    print_progress_ = on;
  }

 private:
  TChain chain_;

 protected:
  /**
   * \brief Build representation of the current event
   *
   * If a derived class overrides this method, it must still call it.
   */
  virtual bool Build();

  TTreeReader reader_;

 private:
  TLorentzVector BuildGenLeptonP4(int index) const;
  TLorentzVector BuildLeptonP4(int index) const;

  bool print_progress_;

  mutable TTreeReaderArray<double> lepton_pt_, lepton_eta_, lepton_phi_;
  mutable TTreeReaderArray<int> lepton_flavor_, lepton_charge_;
  mutable TTreeReaderArray<bool> lepton_tight_id_;
  mutable std::optional<TTreeReaderArray<double>> gen_lepton_pt_,
      gen_lepton_eta_, gen_lepton_phi_;
  mutable std::optional<TTreeReaderArray<int>> gen_lepton_pdg_id_;

  mutable TTreeReaderArray<double> jet_pt_, jet_eta_, jet_phi_, jet_pt_gen_;
  mutable TTreeReaderArray<bool> jet_id_;
  mutable TTreeReaderArray<double> jet_pileup_id_;

  mutable TTreeReaderValue<double> weight_;

  std::vector<bool> lepton_mask_, jet_mask_;
  int64_t events_to_read_, events_read_, events_accepted_;
  TLorentzVector p4_z_;
  std::vector<Jet> jets_;
};

