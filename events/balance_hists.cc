#include <cmath>
#include <cstdlib>
#include <filesystem>
#include <iostream>

#include <boost/program_options.hpp>
#include <TH3.h>
#include <TVector2.h>
#include <yaml-cpp/yaml.h>

#include <BaseReader.h>

namespace fs = std::filesystem;
namespace po = boost::program_options;


int main(int argc, char * const *argv) {
  // Parse command-line options
  po::options_description options{"Supported options"};
  options.add_options()
      ("help,h", "Prints help message.")
      ("sim", "Requests processing of simulation as opposed to data.")
      ("pileup-id", po::value<int>()->default_value(0),
       "Filtering by pileup ID; 0 for no selection, 3 for tight working point.")
      ("output,o", po::value<std::string>()->default_value("hists.root"),
       "Name for output file.");
  po::variables_map options_map;
  po::store(
      po::command_line_parser(argc, argv).options(options).run(),
      options_map);
  if (options_map.count("help")) {
    std::cerr << "Usage: balance_hists [options]\n";
    std::cerr << options << std::endl;
    return EXIT_FAILURE;
  }
  bool const is_sim = options_map.count("sim");

  // Read binning for histograms
  std::vector<double> binning_pt, binning_abs_eta, binning_balance;
  auto const binning_info = YAML::LoadFile("binning.yaml");
  for (auto const &element : binning_info["pt"])
    binning_pt.emplace_back(element.as<double>());
  for (auto const &element : binning_info["abs_eta"])
    binning_abs_eta.emplace_back(element.as<double>());
  for (auto const &element : binning_info["balance"])
    binning_balance.emplace_back(element.as<double>());

  // Create output file and histograms. Most of them are filled only for
  // simulation.
  TFile output_file{options_map["output"].as<std::string>().c_str(),
                    "recreate"};
  TH3D hist_nominal{
      "Nominal", ";Jet p_{T};Jet #eta;p_{T}^{jet} / p_{T}^{Z}",
      int(binning_pt.size()) - 1, binning_pt.data(),
      int(binning_abs_eta.size()) - 1, binning_abs_eta.data(),
      int(binning_balance.size()) - 1, binning_balance.data()};
  hist_nominal.SetDirectory(&output_file);

  TH3D hist_matched{hist_nominal};
  hist_matched.SetName("Matched");
  TH3D hist_unmatched{hist_nominal};
  hist_unmatched.SetName("Unmatched");
  TH3D hist_gen{hist_nominal};
  hist_gen.SetName("Generator");
  TH3D hist_smear{hist_nominal};
  hist_smear.SetName("Smear");
  if (not is_sim)
    for (auto h : {&hist_matched, &hist_unmatched, &hist_gen, &hist_smear})
      h->SetDirectory(nullptr);

  // Create reader for input files
  std::vector<fs::path> input_files;
  if (is_sim)
    input_files = {
      "/pnfs/iihe/cms/store/user/lathomas/DYJetsToLL_M-50_TuneCP5_13TeV-amcatnloFXFX-pythia8/RunIIAutumn18MiniAOD_102X_upgrade2018_realistic_v15_ext2_v1_MC2018_SkimDileptonJERC_July2019_LowJetPt/190731_152454/TOTAL.root",
    };
  else
    input_files = {
      "/pnfs/iihe/cms/store/user/lathomas/DoubleMuon/Run2018A_17Sep2018_v2_Run2018A_SkimDileptonJERC_July2019_LowJetPt/190731_150758/TOTAL.root",
      "/pnfs/iihe/cms/store/user/lathomas/EGamma/Run2018A_17Sep2018_v2_Run2018A_SkimDileptonJERC_July2019_LowJetPt/190731_151920/TOTAL.root",
      "/pnfs/iihe/cms/store/user/lathomas/DoubleMuon/Run2018B_17Sep2018_v1_Run2018B_SkimDileptonJERC_July2019_LowJetPt/190731_151447/TOTAL.root",
      "/pnfs/iihe/cms/store/user/lathomas/EGamma/Run2018B_17Sep2018_v1_Run2018B_SkimDileptonJERC_July2019_LowJetPt/190731_152048/TOTAL.root",
      "/pnfs/iihe/cms/store/user/lathomas/DoubleMuon/Run2018C_17Sep2018_v1_Run2018C_SkimDileptonJERC_July2019_LowJetPt/190731_151619/TOTAL.root",
      "/pnfs/iihe/cms/store/user/lathomas/EGamma/Run2018C_17Sep2018_v1_Run2018C_SkimDileptonJERC_July2019_LowJetPt/190731_152212/TOTAL.root",
      "/pnfs/iihe/cms/store/user/lathomas/DoubleMuon/Run2018D_PromptReco_v2_Run2018D_SkimDileptonJERC_July2019_LowJetPt/190731_151751/TOTAL.root",
      "/pnfs/iihe/cms/store/user/lathomas/EGamma/Run2018D_PromptReco_v2_Run2018D_SkimDileptonJERC_July2019_LowJetPt/190731_152332/TOTAL.root"
    };
  BaseReader reader{input_files, is_sim};
  reader.SetPrintProgress();

  // Fill the histograms
  while (reader.Next()) {
    TLorentzVector const &p4_z = reader.GetZ();
    if (reader.GetJets().empty())
      continue;
    auto const &jet = reader.GetJets().at(0);
    if (jet.pt < binning_pt.front())
      continue;
    if (std::abs(TVector2::Phi_mpi_pi(jet.phi - p4_z.Phi())) < 3.)
      continue;
    if (int(jet.pileup_id) < options_map["pileup-id"].as<int>())
      continue;

    double const pt = jet.pt;
    double const aeta = std::abs(jet.eta);
    double const balance = jet.pt / p4_z.Pt();
    double const weight = (is_sim) ? reader.GetWeight() : 1.;
    
    hist_nominal.Fill(pt, aeta, balance, weight);
    if (not is_sim)
      continue;

    if (jet.pt_gen > 0.)
      hist_matched.Fill(pt, aeta, balance, weight);
    else
      hist_unmatched.Fill(pt, aeta, balance, weight);

    auto const p4_gen_z = reader.ReconstructGenZ();
    if (p4_gen_z)
      hist_gen.Fill(pt, aeta, jet.pt_gen / p4_gen_z->Pt(), weight);

    hist_smear.Fill(pt, aeta, pt / jet.pt_gen, weight);
  }

  output_file.Write();
  output_file.Close();

  return EXIT_SUCCESS;
}

