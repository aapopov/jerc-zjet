# Jet calibration in Z+jet topology

Set up environment with

```sh
. ./env.sh
```

Example command to run the fits:

```sh
cd analysis
./extract_jer.py -y 2018 -o 2018
./plot 2018/results.root -o 2018
```

Commands to build C++ part:

```sh
cd events
mkdir -p build && cd build
cmake -DCMAKE_BUILD_TYPE=Release ..  # Change build type if needed
make
```
